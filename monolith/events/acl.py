from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    # Headers for requests
    headers = {
        "Authorization": PEXELS_API_KEY
    }

    url = "https://api.pexels.com/v1/search"

    params = {
        "query": f"{city}, {state}"
    }
    resp = requests.get(url, headers=headers, params=params).json()

    # Returns the url for the first image in photos
    return resp["photos"][0]["url"]


def get_coords(city, state):
    # insert city and state into url, and send a requests
    to_coordinates_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }
    coordinates = requests.get(to_coordinates_url, params=params)

    # If the coordinates requests is a success
    try:
        coord_data = coordinates.json()

        # tore the latitude and longitude
        lat = coord_data[0]["lat"]
        lon = coord_data[0]["lon"]
        return lat, lon
    # coordinate requests was not succesful
    except(KeyError, IndexError):
        return None


def get_weather_data(city, state):
    # get the latitude and longitude of the location
    lat, lon = get_coords(city, state)

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # insert lat and lon into api search query and request weather data
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather = requests.get(weather_url, params=params)

    # if the weather request is successful
    try:
        weather_data = weather.json()

        # returns the temperature and weather description from the data"
        return {
            "temperature": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"]
        }
    # weather request was not successful
    except(KeyError, IndexError):
        return None
